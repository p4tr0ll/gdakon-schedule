import {
    Card,
    CardHeader,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
} from "@mui/material";
import { useTranslation } from "react-i18next";

import { LanguageSettings } from "../Settings/LanguageSettings";
import { TimeTravelSettings } from "../Settings/TimeTravelSettings";
import { OnlineCheck } from "../Home/OnlineCheck";
import { ServiceWorker } from "../Common/ServiceWorker";

import GitIcon from "~icons/mdi/git";
import BuildIcon from "~icons/mdi/build";
import { name, version } from "~build/package";
import buildDate from "~build/time";

export const AboutCard = () => {
    const { t } = useTranslation("About");
    return (
        <Card>
            <CardHeader title={t("AboutCard.title")} />
            <ServiceWorker />
            <OnlineCheck showIfOnline />
            <List>
                <ListItem>
                    <ListItemIcon>
                        <GitIcon fontSize={"1.8rem"} />
                    </ListItemIcon>
                    <ListItemText primary={name} secondary={version} />
                </ListItem>
                <ListItem>
                    <ListItemIcon>
                        <BuildIcon fontSize={"1.8rem"} />
                    </ListItemIcon>
                    <ListItemText
                        primary={buildDate.toISOString()}
                        secondary={"Build Timestamp"}
                    />
                </ListItem>
                <LanguageSettings />
                <TimeTravelSettings />
            </List>
        </Card>
    );
};

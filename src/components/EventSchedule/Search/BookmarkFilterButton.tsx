import { Button } from "@mui/material";

import { useEventFilter } from "../EventFilter.Provider";

import AddBookmarkIcon from "~icons/mdi/bookmark-outline";
import BookmarkIcon from "~icons/mdi/bookmark";

export const BookmarkFilterButton = () => {
    const { filters, toggleBookmarked } = useEventFilter();
    return (
        <Button
            onClick={toggleBookmarked}
            sx={{
                color: filters.bookmarked ? "warning" : undefined,
            }}
        >
            {filters.bookmarked ? (
                <BookmarkIcon fontSize={"1.4rem"} />
            ) : (
                <AddBookmarkIcon fontSize={"1.4rem"} />
            )}
        </Button>
    );
};
